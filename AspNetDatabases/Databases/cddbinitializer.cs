﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using AspNetDatabases.Models;

namespace AspNetDatabases.Databases
{
    public class CdDbInitializer : DropCreateDatabaseIfModelChanges<CdDbContext>
    {
        protected override void Seed(CdDbContext context)
        {
            var CdDbItems = new List<CdDb>
            {
                new CdDb{ ID = 0, Title = "Desire", Artist = "Bob Dylan" },
                new CdDb{ ID = 1, Title = "Crossroads", Artist = "Eric Clapton"}
            };
            CdDbItems.ForEach(m => context.cddb.Add(m));
            context.SaveChanges();
        }
    }
}